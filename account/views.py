from django.contrib.auth import authenticate, login, logout
from rest_framework import viewsets, mixins
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework.decorators import list_route
from rest_framework.authentication import BasicAuthentication
from rest_framework.exceptions import AuthenticationFailed, PermissionDenied, NotAuthenticated

from .permissions import IsSuperAdmin, IsUserAccount
from .authenticate import CsrfExemptSessionAuthentication
from .serializers import LogInSerializer, UserRegisterSerializer, MachineRegisterSerializer, MachineListSerializer
from .models import Account, UserTemp


class RegisterViewSet(viewsets.GenericViewSet):
    queryset = Account.objects.all()
    serializer_class = UserRegisterSerializer

    action_serializers = {
        'user_register': UserRegisterSerializer,
        'machine_register': MachineRegisterSerializer,
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    def get_permissions(self):
        if self.action == 'machine_register':
            self.permission_classes = [IsSuperAdmin, ]
        else:
            self.permission_classes = [AllowAny, ]
        return super(RegisterViewSet, self).get_permissions()


    @list_route(methods=['post'], url_path='user')
    def user_register(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            usertemp = UserTemp.objects.filter(phone_number=serializer.data['phone_number']).first()
            if usertemp:
                balance = usertemp.balance
                usertemp.delete()
            else:
                balance = 0
            account = Account.objects.create_user(
                username=serializer.data['username'],
                balance=balance,
                phone_number=serializer.data['phone_number']
            )
            account.set_password(serializer.data['password'])
            account.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='machine')
    def machine_register(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            account = Account.objects.create_machine(
                username=serializer.data['username'],
                latitude=serializer.data['latitude'],
                longtitude=serializer.data['longtitude'],
                address=serializer.data['address']
            )
            account.set_password(serializer.data['password'])
            account.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LoginViewSet(mixins.CreateModelMixin,
				   viewsets.GenericViewSet):

    queryset = Account.objects.all()
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication,)
    serializer_class = LogInSerializer

    def create(self, request, *args, **kwargs):
        import re
        if request.user.is_authenticated():
            raise PermissionDenied('Please logout.')

        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            username = serializer.data.get('username', None)
            password = serializer.data.get('password')

            user = authenticate(username=username, password=password)
            if not user:
                raise AuthenticationFailed("This email or password is not valid.")

            login(request, user)
            request.session['ip'] = request.META.get('REMOTE_ADDR')
            request.session.set_expiry(3153600000)

            money = Account.objects.filter(username=username).first()

            return Response({"name": username , "balance": money.balance , "phone-number": money.phone_number},status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LogoutView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        if request.user.is_authenticated():
            logout(request)
            return Response(status=status.HTTP_200_OK)
        else:
            raise NotAuthenticated('Please login.')



class MachineViewSet(mixins.ListModelMixin,
                     viewsets.GenericViewSet):

    queryset = Account.objects.all()
    serializer_class = MachineListSerializer
    permission_classes = (IsUserAccount,)

    action_serializers = {
        'list': UserRegisterSerializer,
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()


    def list(self, request):
        machine_list = Account.objects.filter(role=2)
        response = MachineListSerializer(machine_list, many=True).data
        return Response(response)