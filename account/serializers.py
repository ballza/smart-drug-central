from rest_framework import serializers
from .models import Account

class LogInSerializer(serializers.Serializer):

    username = serializers.CharField(max_length=255)
    password = serializers.CharField(min_length=4)


class UserRegisterSerializer(serializers.Serializer):

    username = serializers.CharField(max_length=255)
    password = serializers.CharField(min_length=4)
    phone_number = serializers.CharField(max_length=20)


class MachineRegisterSerializer(serializers.Serializer):

    username = serializers.CharField(max_length=255)
    password = serializers.CharField(min_length=4)
    latitude = serializers.CharField(max_length=255)
    longtitude = serializers.CharField(max_length=255)
    address = serializers.CharField(min_length=1)


class MachineListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Account
        fields = ('id', 'username', 'latitude', 'longtitude', 'address')