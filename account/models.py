from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin)


class AccountManager(BaseUserManager):

    def _create_user(self, username, password, **extra_fields):
        user = self.model(
            username=username,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, password=None, **extra_fields):
        user = self._create_user(username=username,
                                 password=password,
                                 **extra_fields)
        user.role = 1
        user.save(using=self._db)
        return user

    def create_machine(self, username, password=None, **extra_fields):
        user = self._create_user(username=username,
                                 password=password,
                                 **extra_fields)
        user.role = 2
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        user = self._create_user(username=username,
                                 password=password)
        user.is_admin = True
        user.is_superuser = True
        user.role = 1
        user.save(using=self._db)
        return user


class Account(AbstractBaseUser, PermissionsMixin):
    ROLE = (
        (1, "User"),
        (2, "Machine")
    )

    username = models.CharField(max_length=200, unique=True)
    phone_number = models.CharField(max_length=20, null=True, blank=True)
    balance = models.IntegerField(default=0, null=True)

    latitude = models.CharField(max_length=100, null=True, blank=True)
    longtitude = models.CharField(max_length=100, null=True, blank=True)

    address = models.TextField(null=True, blank=True)

    role = models.IntegerField(choices=ROLE, default=1)
    is_admin = models.BooleanField(default=False)
    # is_active = models.BooleanField(default=True)
    # last_active = models.DateTimeField(_('last active'), blank=True, null=True)
    # date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = AccountManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.username

    @property
    def is_staff(self):
        return self.is_admin

    def get_short_name(self):
        return self.username

    def get_full_name(self):
        return self.username


class UserTemp(models.Model):

    phone_number = models.CharField(max_length=20)
    balance = models.PositiveIntegerField(default=0)

    def __str__(self):
        return str(self.phone_number)