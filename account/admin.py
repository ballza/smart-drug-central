from django.contrib import admin
from .models import Account, UserTemp
# Register your models here.

class AccountAdmin(admin.ModelAdmin):
	list_display = ('id', 'username', 'get_role')

	def get_role(self, account):
		if account.is_admin:
			return "User Admin"
		return "User" if account.role == 1 else "Machine"
	get_role.short_description = "Role"


class UserTempAdmin(admin.ModelAdmin):
	list_display = ('id', 'phone_number', 'balance')

		
admin.site.register(Account, AccountAdmin)
admin.site.register(UserTemp, UserTempAdmin)