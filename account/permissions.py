from rest_framework.permissions import BasePermission


class IsSuperAdmin(BasePermission):

    def has_permission(self, request, view):
        if not request.user.is_authenticated():
            return False
        return request.user.is_admin


class IsUserAccount(BasePermission):

    def has_permission(self, request, view):
        if not request.user.is_authenticated():
            return False
        return (request.user.role == 1) or request.user.is_admin


class IsMachineAccount(BasePermission):

    def has_permission(self, request, view):
        if not request.user.is_authenticated():
            return False
        return (request.user.role == 2) or request.user.is_admin