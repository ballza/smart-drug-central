from django.contrib import admin
from .models import *


class OrderListAdmin(admin.ModelAdmin):
	list_display = ('id', 'get_drug', 'quantity', 'get_order')

	def get_drug(self, orderlist):
		return orderlist.drug.name
	get_drug.short_description = "Drug"

	def get_order(self, orderlist):
		return orderlist.order.id
	get_order.short_description = "Order ID"


class OrderAdmin(admin.ModelAdmin):
	list_display = ('id', 'date', 'get_user', 'get_machine', 'is_active')

	def get_user(self, order):
		return order.user.username
	get_user.short_description = "Username"

	def get_machine(self, order):
		return order.machine.username
	get_machine.short_description = "Machine"


class DrugAdmin(admin.ModelAdmin):
	list_display = ('id', 'id_tray','name', 'count', 'get_machine', 'is_active')

	def get_machine(self, drug):
		if not drug.machine:
			return ""
		return drug.machine.username
class SymptomAdmin(admin.ModelAdmin):
	list_display = ('id', 'name')

class BagAdmin(admin.ModelAdmin):
	list_display = ('id', 'expired_Date')



admin.site.register(Order, OrderAdmin)
admin.site.register(Drug, DrugAdmin)
admin.site.register(Order_list, OrderListAdmin)
admin.site.register(tray_central)
admin.site.register(Symptom, SymptomAdmin)
admin.site.register(Bag, BagAdmin)
# Register your models here.
