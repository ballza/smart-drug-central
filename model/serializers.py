from rest_framework import serializers
from .models import *


class UpdateMoneySerializer(serializers.Serializer):

	phone = serializers.CharField(max_length=20)
	balance = serializers.IntegerField()


class DrugSerializer(serializers.ModelSerializer):
	expired_Date = serializers.SerializerMethodField()
	class Meta:
		model = Drug
		fields = ('id','name', 'id_tray','count','price','description','image','expired_Date')

	def get_expired_Date(self, drug):
		return drug.bag.expired_Date


class HistoryOrderSerializer(serializers.ModelSerializer):
	date = serializers.SerializerMethodField()
	otp = serializers.SerializerMethodField()

	class Meta:
		model = Order_list
		fields = ('date', 'drug', 'quantity', 'otp')

	def get_date(self, order_list):
		return order_list.order.date

	def get_otp(self,order_list):
		return order_list.order.otp

class ActiveOrderSerializer(serializers.ModelSerializer):
	order_list = serializers.SerializerMethodField()
	
	class Meta:
		model = Order
		fields = ('date', 'otp', 'order_list')

	def get_order_list(self, order):
		response = []
		for obj in Order_list.objects.filter(order=order):
			response.append({
				'drug': obj.drug.name,
				'quantity': obj.quantity
			})
		return response

class DrugCountSerializer(serializers.Serializer):

	drug_id = serializers.IntegerField(required=True)
	count = serializers.IntegerField(required=True)


class OrderListSerializer(serializers.Serializer):
	
	machine_id = serializers.IntegerField(required=True)
	order_list = serializers.ListField(
	   child=DrugCountSerializer()
	)


class BuySerializer(serializers.Serializer):

	otp = serializers.CharField(max_length=4, min_length=4, required=True)


class UpdateOrderListSerializer(serializers.ModelSerializer):
	id_tray = serializers.SerializerMethodField()

	class Meta:
		model = Order_list
		fields = ('id_tray', 'quantity')

	def get_id_tray(self, order_list):
		return order_list.drug.id_tray


class UpdateOrderSerializer(serializers.ModelSerializer):
	order_list = serializers.SerializerMethodField()

	class Meta:
		model = Order
		fields = ('otp','date', 'order_list')

	def get_order_list(self, order):
		order_list = Order_list.objects.filter(order=order)
		return UpdateOrderListSerializer(order_list, many=True).data


class UpdateDrugDetailSerializer(serializers.Serializer):
	
	id_tray = serializers.IntegerField(required=True)
	count = serializers.IntegerField(required=True)

	
class UpdateDrugSerializer(serializers.Serializer):

	drug_list = serializers.ListField(
	   child=UpdateDrugDetailSerializer()
	)

class TopUpmoneySerializer(serializers.Serializer):

	phone = serializers.CharField(max_length=10, required=True)
	cash = serializers.IntegerField(required=True)

class ReduceDrugSerializer(serializers.Serializer):

	id_tray = serializers.IntegerField(required=True)
	count = serializers.IntegerField(required=True)

class DeleteOrderSerializer(serializers.Serializer):

	otp = serializers.CharField(required=True)
		