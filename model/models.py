from django.db import models
from account.models import Account


class Order(models.Model):
	otp = models.CharField(max_length=6,null=True, blank=True)
	date = models.DateField()
	user = models.ForeignKey(Account, null=True, related_name='user', on_delete=models.CASCADE)
	machine = models.ForeignKey(Account, null=True, related_name='machine', on_delete=models.CASCADE)
	is_active = models.BooleanField(default=True)

	def __str__(self):
		return self.user.username + " | " + str(self.date)

class Symptom(models.Model):

	name = models.CharField(max_length=255,null=True, blank=True)

	def __str__(self):
		return self.name

class Bag(models.Model):

	expired_Date = models.DateTimeField()

	def __str__(self):
		return str(self.expired_Date)
		

class Drug(models.Model):


	machine = models.ForeignKey(Account, null=True)
	bag = models.ForeignKey(Bag,on_delete=models.CASCADE ,null=True)
	name = models.CharField(max_length=255,null=True, blank=True)
	id_tray = models.PositiveIntegerField(default=0)
	count = models.PositiveIntegerField(default=0)
	price = models.PositiveIntegerField(default=0)
	description = models.TextField(null=True,blank=True)
	symptom = models.ForeignKey(Symptom,on_delete=models.CASCADE ,null=True)
	image = models.ImageField(upload_to='model/%Y/%m', null=True, blank=True)
	is_active = models.BooleanField(default=True)

	def __str__(self):
		status = " | active" if self.is_active else " | unactive"
		return self.name + status


		
class Order_list(models.Model):

	order = models.ForeignKey(Order,on_delete=models.CASCADE)
	drug = models.ForeignKey(Drug,on_delete=models.CASCADE)
	quantity = models.PositiveIntegerField(default=0)
	is_active = models.BooleanField(default=True)

	def __str__(self):
		return "drug: " + str(self.drug.name) + " order_id:" + \
			   str(self.order.id) + " " + str(self.order.date)


class tray_central(models.Model):

	expiration_date = models.DateTimeField()

