from rest_framework import viewsets, mixins
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.decorators import detail_route, list_route

from account.permissions import IsUserAccount, IsMachineAccount
from account.models import Account, UserTemp
from .models import *
from .serializers import *
# Create your views here.

class DrugViewSet_central(viewsets.GenericViewSet):
    queryset = Drug.objects.all()
    serializer_class = DrugSerializer
    permission_classes = (IsUserAccount,)

    @detail_route(methods=['get'], url_path='list_central')
    def all_drug(self,request, pk=None):
        drug = Drug.objects.filter(machine__id=pk)
        serializer = DrugSerializer(drug, many=True)
        return Response(serializer.data)


    @detail_route(methods=['get'], url_path='list_symptom')
    def all_symptom(self,request, pk=None):
        response = []
        for symptom in Symptom.objects.all():
            response.append({
                "symptom": symptom.name,
                "drug_list": DrugSerializer(Drug.objects.filter(machine__id=pk, symptom=symptom), many=True).data
            })


        return Response(response)

class OrderViewSet_central(viewsets.GenericViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderListSerializer
    permission_classes = (IsUserAccount,)

    action_serializers = {
        'app_make_order': OrderListSerializer,
        'machine_buy': BuySerializer,
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()


    def get_permissions(self):
        user_group = ['app_make_order','app_HistoryOrder']
        machine_group = ['machine_buy']
        if self.action in user_group:
            self.permission_classes = [IsUserAccount, ]
        elif self.action in machine_group:
            self.permission_classes = [IsMachineAccount, ]
        else:
            self.permission_classes = [AllowAny, ]
        return super(OrderViewSet_central, self).get_permissions()


    @list_route(methods=['post'], url_path='app-make-order')
    def app_make_order(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            from random import randint
            from datetime import datetime

            input_order = serializer.data["order_list"]
            sum_price = 0
            for order in input_order:
                drug = Drug.objects.filter(id=order["drug_id"]).first()
                sum_price += (drug.price * order["count"])

            if sum_price > request.user.balance:
                message = "your balance is " + str(request.user.balance) + " baht and total drug price is " +\
                           str(sum_price) + " baht : Not Enough !!!"
                return Response({"error": message}, status=status.HTTP_400_BAD_REQUEST)    

            user = Account.objects.filter(id=request.user.id).first()
            user.balance -= sum_price
            user.save()        

            check = True
            while check:
                otp = ""        
                for i in range(6):
                    otp += str(randint(0,9))
                if not Order.objects.filter(is_active=True, otp=otp).exists():
                    check = False

            machine = Account.objects.filter(id=serializer.data["machine_id"]).first()
            order = Order.objects.create(otp=otp, date=datetime.now(),
                                         user=request.user, machine=machine)
            order.save()

            for order_input in serializer.data['order_list']:
                drug = Drug.objects.filter(id=order_input['drug_id']).first()

                Order_list.objects.create(
                    order=order,
                    drug=drug,
                    quantity=order_input['count']
                ).save()
            return Response({"otp": otp, 'balance': user.balance},status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='machine_buy')
    def machine_buy(self,request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():

            order = Order.objects.filter(otp=serializer.data['otp'], user=request.user, is_active=True).first()
            if not order:
                return Response({'your otp is wrong'}, status=status.HTTP_400_BAD_REQUEST)

            for order_list in Order_list.objects.filter(order=order):
                drug = Drug.objects.filter(id=order_list.drug.id).first()
                drug.count -= order_list.quantity
                print(drug.count)
                drug.save(update_fields=['count'])

            order.is_active = False
            order.save()
            return Response({'Success'}, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


    @list_route(methods=['get'], url_path='app-HistoryOrder')
    def app_HistoryOrder(self,request):
        history_order = Order.objects.filter(user=request.user, is_active=False)
        if not history_order:
            return Response({'your do not create order'}, status=status.HTTP_400_BAD_REQUEST)


        order_list = Order_list.objects.filter(order__in=history_order)
        response = HistoryOrderSerializer(order_list, many=True).data
        return Response(response, status=status.HTTP_200_OK)

    @list_route(methods=['get'], url_path='app-ActiveOrder')
    def app_ActiveOrder(self,request):
        Active_order = Order.objects.filter(user=request.user, is_active=True)
        if not Active_order:
            return Response({'your do not active order'}, status=status.HTTP_400_BAD_REQUEST)


        response = ActiveOrderSerializer(Active_order, many=True).data
        return Response(response, status=status.HTTP_200_OK)


class ServerViewSet(viewsets.GenericViewSet):
    queryset = Drug.objects.all()
    permission_classes = (IsMachineAccount,)
    serializer_class = UpdateDrugSerializer

    action_serializers = {
        'receive_update_drug': UpdateDrugSerializer,
        'receive_update_money': UpdateMoneySerializer,
        'receive_topup_money': TopUpmoneySerializer,
        'receive_reduce_drug': ReduceDrugSerializer,
        'receive_delete_order': DeleteOrderSerializer

    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    @list_route(methods=['get'], url_path='update-order')
    def receive_update_data(self, request):
        order = Order.objects.filter(machine=request.user, is_active=True)
        response = UpdateOrderSerializer(order, many=True).data
        for obj in order:
            obj.is_active = False
            obj.save(update_fields=['is_active'])
        return Response(response)

    @list_route(methods=['post'], url_path='update-drug')
    def receive_update_drug(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            for drug_input in serializer.data["drug_list"]:
                drug = Drug.objects.filter(id_tray=drug_input["id_tray"],
                                           machine=request.user,
                                           is_active=True).first()
                if drug:
                    drug.count = drug_input["count"]
                    drug.save(update_fields=["count"])
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


    @list_route(methods=['post'], url_path='update-money')
    def receive_update_money(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            phone = serializer.data['phone']
            balance = serializer.data['balance']

            user = Account.objects.filter(phone_number=phone).first()
            if user:
                user.balance += balance
                user.save()
            else:
                newuser = UserTemp.objects.create(phone_number=phone,
                                            balance=balance)
                newuser.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='topup-money')
    def receive_topup_money(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            phone = serializer.data['phone']
            cash= serializer.data['cash']

            user = Account.objects.filter(phone_number=phone).first()
            if user:
                user.balance += cash
                user.save()
            else:
                newuser = UserTemp.objects.create(phone_number=phone,
                                            balance=cash)
                newuser.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='reduce-drug')
    def receive_reduce_drug(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            id_tray = serializer.data['id_tray']
            count = serializer.data['count']

            update = Drug.objects.filter(id_tray=id_tray).first()
            if not update:
                return Response({'wrong id_tray'},status=status.HTTP_400_BAD_REQUEST)
            update.count = count
            update.save()
            return Response({'Success'},status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='delete-order')
    def receive_delete_order(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            otp = serializer.data['otp']

            delete = Order.objects.filter(otp=otp,is_active=False).first()
            if not delete:
                return Response({'no order'},status=status.HTTP_400_BAD_REQUEST)
            delete.delete()
            return Response({'Success'},status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
