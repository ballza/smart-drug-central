

from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from rest_framework_swagger.views import get_swagger_view
from rest_framework.routers import DefaultRouter
from model.views import *
from account.views import LoginViewSet, LogoutView, RegisterViewSet, MachineViewSet


schema_view = get_swagger_view(title='CentralSmartDrug API')
router = DefaultRouter()

router.register(r'drug', DrugViewSet_central)
router.register(r'order', OrderViewSet_central)
router.register(r'login', LoginViewSet)
router.register(r'register', RegisterViewSet)
router.register(r'machine', MachineViewSet)
router.register(r'server', ServerViewSet)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/$', schema_view),
    url(r'^api/', include(router.urls)),
    url(r'^api/logout/', LogoutView.as_view()),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)##+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
